package com.caro;

enum Regex {
    BOOLEAN("\true|\false"),
    CONTEXT("\\[.+\\]"),
    DATE("\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])"),
    IN("in"),
    NUMBER("\\d+"),
    QUERY("(\\/\\S+)"),
    RESOURCE("(\\/\\S+)|([a-z]+[A-Z]+[a-zA-Z]*)"),
    THREAD_ID("\\(.+\\)"),
    TIME("([01]\\d|2[0123])\\:[012345]\\d\\:[012345]\\d\\,\\d\\d\\d");

    private String value;

    Regex(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
