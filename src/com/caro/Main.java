package com.caro;

public class Main {

    public static void main(String[] args) {
        final long startTime = System.currentTimeMillis();
        LogFileParser logFileParser = new LogFileParser();
        String logFile = "";
        int numberOfResults = 0;
        boolean displayHelp = false;

        for (String arg : args) {
            if (arg.matches("\\d+")) {
                numberOfResults = Integer.parseInt(arg);
            } else if (arg.equals("-h")) {
                displayHelp = true;
            } else if (arg.matches("[a-zA-Z]+.log")) {
                logFile = arg;
            }
        }

        if (displayHelp || !(numberOfResults > 0) || logFile.length() <= 0) {
            System.out.println("The parameters for running the program" +
                    " are the name of the log file " +
                    "and the amount of results to be shown. \n" +
                    "For example you can try running the program as follows: " +
                    "java -jar assignment.jar timing.log 10\n\n" +
                    "The program will print out top n (value of n is passed as program argument) " +
                    "resources with highest average request duration.");
            System.exit(0);
        }

        logFileParser.parseLogFileAndCalculateAverages(numberOfResults, logFile);

        final long endTime = System.currentTimeMillis();
        System.out.println("- - -");
        System.out.println("\nProgram execution time: " + (endTime - startTime) + " ms\n");

    }
}
