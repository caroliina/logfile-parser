package com.caro;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.*;

class LogFileParser {

    private Utils utils = new Utils();
    private JSONArray jsonArray = new JSONArray();
    private Map<String, ArrayList<Long>> resourceDurationsMap = new HashMap<>();
    private Map<String, Double> resourceAverageDurationMap = new HashMap<>();
    private Map<String, Integer> histogram = new HashMap<>();
    private int amountOfLogs = 0;

    void parseLogFileAndCalculateAverages(int numberOfResults, String logFile) {
        try {
            FileInputStream stream = new FileInputStream(logFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                parseLineToJSON(strLine);
            }
            stream.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }

        Map<String, Double> averages = utils.mapResourcesWithAverages(resourceDurationsMap, resourceAverageDurationMap);
        utils.printResourcesWithAverages(numberOfResults, averages);
        utils.printHistogram(histogram, amountOfLogs);
    }

    private void parseLineToJSON(String strLine) throws ParseException {
        JSONObject obj = new JSONObject();
        List<String> logLineAsList = Arrays.asList(strLine.split("\\s+"));
        String timeString = "";
        String resourceName = "";

        for (String element : logLineAsList) {
            if (element.matches(Regex.BOOLEAN.value())) {
                obj.put("Boolean", element);
                logLineAsList.remove(element);
                continue;
            }
            if (element.matches(Regex.DATE.value())) {
                String date = utils.formatDate(element);
                timeString = "" + date;
                obj.put("Date", date);
            } else if (element.matches(Regex.TIME.value())) {
                String time = utils.formatTime(element);
                timeString = timeString + " " + time;
                obj.put("Time", time);
            } else if (element.matches(Regex.THREAD_ID.value())) {
                obj.put("Thread ID", element);
            } else if (element.matches(Regex.CONTEXT.value())) {
                obj.put("Optional user context", element);
            } else if (element.matches(Regex.RESOURCE.value())) {
                resourceName = utils.filterActionFromQueryString(element);
                obj.put("Resource", resourceName);
            } else if (element.matches(Regex.IN.value())) {
                int indexOfIn = logLineAsList.indexOf(element);
                int indexOfDuration = indexOfIn + 1;
                int indexOfPayload = indexOfIn - 1;
                String payload = logLineAsList.get(indexOfPayload);
                String duration = logLineAsList.get(indexOfDuration);
                if (payload.matches(Regex.NUMBER.value())) {
                    obj.put("Data payload elements", Long.parseLong(payload));
                }
                if (duration.matches(Regex.NUMBER.value())) {
                    obj.put("Request duration", Long.parseLong(duration));
                    resourceDurationsMap = utils.addResourceDurationToMap(resourceDurationsMap, resourceName, duration);
                }
            }
        }

        histogram = utils.addTimeAmountToMap(histogram, timeString);
        jsonArray.add(obj);
        amountOfLogs++;
    }

}
