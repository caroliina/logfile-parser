package com.caro;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Utils {

    Map<String, Double> mapResourcesWithAverages(Map<String, ArrayList<Long>> map, Map<String, Double> avgMap) {
        map.forEach((resource, durations) -> {
            OptionalDouble average = durations
                    .stream()
                    .mapToDouble(a -> a)
                    .average();
            /*
             *   Discarding the URI + query string logs that did not have any action parameter
             */
            if (!resource.contains("/")) {
                avgMap.put(resource, average.getAsDouble());
            }
        });
        return avgMap;
    }

    Map<String, Integer> addTimeAmountToMap(Map<String, Integer> map, String timeString) {
        map.putIfAbsent(timeString, 0);
        map.put(timeString, map.get(timeString) + 1);
        return map;
    }

    Map<String, ArrayList<Long>> addResourceDurationToMap(Map<String, ArrayList<Long>> map, String resource, String duration) {
        map.putIfAbsent(resource, new ArrayList<>());
        map.get(resource).add(Long.parseLong(duration));
        return map;
    }

    String formatTime(String element) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss,SSS");
        sdf.applyPattern("HH:mm");
        return sdf.format(sdf.parse(element));
    }

    String formatDate(String element) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(sdf.parse(element));
    }

    String filterActionFromQueryString(String element) {
        Pattern actionPattern = Pattern.compile(".*action=([A-Z]+).*");
        Matcher actionMatcher = actionPattern.matcher(element);
        if (element.matches(Regex.QUERY.value()) && actionMatcher.matches()) {
            return actionMatcher.group(1);
        } else {
            return element; // Keep the whole URI + query string if has no action
        }
    }

    void printResourcesWithAverages(int numberOfResults, Map<String, Double> map) {
        System.out.println("\nShowing " + numberOfResults +
                " resources with highest average request duration" +
                " retrieved from the log file. \n");
        System.out.printf("%-30.30s  %-30.30s%n", "RESOURCE NAME", "REQUEST DURATION IN MS");
        System.out.println("- - -");

        map.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed())
                .limit(numberOfResults)
                .forEach(item -> System.out.printf("%-30.30s  %-30.30s%n",
                        item.getKey(), item.getValue()));
    }

    void printHistogram(Map<String, Integer> map, int amountOflogs) {
        System.out.println("\n\n");
        System.out.printf("%-30.30s  %-30.30s%n", "TIMESTAMP (yyyy-MM-DD HH:mm)",
                "AMOUNT OF REQUESTS (" + amountOflogs + ")");
        System.out.println("- - -");
        map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(item -> System.out.printf("%-30.30s  %-40.40s%n",
                        item.getKey(), generateBars(item.getValue(), amountOflogs)));
    }

    private String generateBars(int amount, int amountOflogs) {
        int percentage = (amount * 100) / amountOflogs;
        String bars = "";
        for (int i = 0; i < percentage; i++) {
            bars = bars + "∎";
        }
        return "[" + amount + "] " + bars + " (" + percentage + "%)";
    }

}
