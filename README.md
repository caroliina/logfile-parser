# Logfile parser
A program that parses the log file and extracts, aggregates and presents the data from it.

## Technologies
* Java 8 SE
* Ant 1.9

## Introduction
The log file **timing.log** contains request durations. The file format is best explained with the following examples:

```
2015-08-19 00:00:01,049 (http--0.0.0.0-28080-405) [] /checkSession.do in 187
2015-08-19 00:00:01,963 (http--0.0.0.0-28080-245) [] /checkSession.do in 113
2015-08-19 00:00:02,814 (http--0.0.0.0-28080-245) [CUST:CUS5T27233] /substypechange.do?msisdn=300501633574 in 17
2015-08-19 00:00:03,260 (http--0.0.0.0-28080-245) [CUST:CUS5T27233] /mainContent.do?action=TOOLS&contentId=main_tools in 5
```
that contains:
- date
- timestamp
- thread-id (in brackets)
- optional user context (in square brackets)
- URI + query string
- string "in"
- request duration in milliseconds

..and..

```
2015-08-19 00:04:45,212 (http--0.0.0.0-28080-405) [] updateSubscriptionFromBackend 300445599231 in 203
2015-08-19 00:04:45,259 (http--0.0.0.0-28080-405) [ASP CUST:CUS5T27233] getPermission 300445599231 in 32
```

which contains:
- date
- timestamp
- thread-id (in brackets)
- optional user context (in square brackets)
- requested resource name (one string)
- data payload elements for resource (0..n elements)
- string "in"
- request duration in milliseconds

## Running the program
Create the .jar file (although there is one in the /dist folder already):

`ant -f build.xml`

Running the program (when running it with **-h** argument it shows helpful information)

`java -jar assignment.jar timing.log 10`

## Program outputs
- Printing out top n (exact value of n is passed as program argument) resources with highest average request duration.
- Drawing histogram of hourly number of requests.
- Printing out number of (milli)seconds the program run


